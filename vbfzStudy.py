#!/bin/python

###-----------------------------------------------------------------------------
#from enum import IntEnum
import array
from operator import itemgetter, attrgetter, methodcaller
#try:
#    import numpy
#except:
#    print("ERROR Failed to import numpy")
#    exit(1)

#-- import user modules
#from anaTools import *
import anaTools

###-----------------------------------------------------------------------------
### Setting ROOT
# Set up ROOT library
import ROOT
#from ROOT import TCanvas, TH1F, TLorentzVector
#from ROOT import *
ROOT.gROOT.Macro( '$ROOTCOREDIR/scripts/load_packages.C' )

# Initialize the xAOD infrastructure: 
if(not ROOT.xAOD.Init().isSuccess()): print "Failed xAOD.Init()"

###-----------------------------------------------------------------------------
### Opening samples for analysis

# Set up the input files:
ds_fileName = "/afs/cern.ch/work/c/cchau/VBFZ/devArea/powhegZjj/batch/mc_prod/reco/localrun/DAOD_TRUTH1.powheg_prod4.aod.root"

treeName = "CollectionTree" # default when making transient tree anyway

ds_file = ROOT.TFile.Open(ds_fileName)
#Make the "transient tree":
tChain = ROOT.xAOD.MakeTransientTree( ds_file, treeName)

###-----------------------------------------------------------------------------
PT_LEP = 10000.0
PT_JET = 20000.0

#-- Testing
##class DressedLepton:
#t_lep = DressedLepton(0)
#t_lep.setParameters(15000., 1.2, 2.6, 0.5, 15000., -11, 0.1)
##lep_tlv = t_lep.getP4()
#print("t_lep pt: %g" % t_lep.getP4().Pt())
#print("t_lep eta: %g" % t_lep.getP4().Eta())
#print("t_lep phi: %g" % t_lep.getP4().Phi())
#print("t_lep energy: %g" % t_lep.getP4().E())
#print("t_lep mass: %g" % t_lep.getP4().M())
#print("t_lep px: %g, py: %g, pz: %g" % (t_lep.getP4().Px(), t_lep.getP4().Py(), t_lep.getP4().Pz()) )

###-----------------------------------------------------------------------------
#-- Loop over samples

#-- Number of samples to analyze
NSAMPLES = 1

#-- Containers for storing event info
weightColl = []
#-- Conatiners for storing good electrons, muons and jets
leptonColl = []
jetColl = []
gapJetColl = []

#-- Print some information:
NENTRIES = tChain.GetEntries()
print( "Number of input events: %s" % tChain.GetEntries() )

totalWeight = 0

# Run on few events to test the macro
NENTRIES = 10
for entry in xrange(0,NENTRIES):
    tChain.GetEntry( entry )
    v_mcWeight=tChain.EventInfo.mcEventWeights()
    mcWeight=tChain.EventInfo.mcEventWeight()
    #h_mcWeight.Fill(mcWeight/10000.)
    totalWeight += mcWeight

#    if entry < 10:
#        print( "Processing run #%i, event #%i" % ( tChain.EventInfo.runNumber(), tChain.EventInfo.eventNumber() ) )
#        for i in xrange(v_mcWeight.size()):
#            print( "... mc weight=%g; weight=%g" %  ( v_mcWeight.at(i), mcWeight ) )
#        print( "Number of electrons: %i" % len( tChain.TruthElectrons ) )
#        # loop over electron collection
#        for i in xrange( tChain.TruthElectrons.size()):
#           el = tChain.TruthElectrons.at(i)
#           print( "  Electron pt = %g, eta = %g, phi = %g" %  ( el.pt(),el.eta(), el.phi() ) )
#           pass # end for loop over electron collection
#        print( "Number of jets: %i" % len( tChain.AntiKt4TruthWZJets ) )
#        for i in xrange( tChain.AntiKt4TruthWZJets.size()):
#           jet = tChain.AntiKt4TruthWZJets.at(i)
#           print( "  Jet trackParticle pt = %g, eta = %g, phi = %g" %  ( jet.pt(),jet.eta(), jet.phi() ) )
#           pass # end for loop over electron collection

    #-- Select good vertices
    n_vtx = 0
    ind_PV = 0
    primary_vertex = tChain.TruthVertices.at(0)
    #print("vertex size: {} and vertex length: {}".format(tChain.TruthVertices.size(), len(tChain.TruthVertices)))
    #print("vertex type: {} and vertex identifier: {}".format(tChain.TruthVertices[0].type(), ROOT.xAOD.Type.Vertex))

    for vitr in range(tChain.TruthVertices.size()):
        #print("vertex type: {} and vertex identifier: {}".format(tChain.TruthVertices[vitr].type(), ROOT.xAOD.Type.Vertex))
        if tChain.TruthVertices[vitr].type() == ROOT.xAOD.Type.TruthVertex:
            n_vtx = n_vtx + 1
            primary_vertex = tChain.TruthVertices.at(vitr)
            ind_PV = vitr

    if n_vtx < 1:
        print("ERROR  No valid primary vertex in the event %i" % (tChain.EventInfo.eventNumber()) )

    #-- Select muons or electrons coming from leptons or weak boson
    leptons = []
    photons = []
    anaTools.identifyTruthLeptonsAndPhotons(leptons, photons, tChain.TruthParticles)
    #print("Number of leptons, {}, and photons, {}, identified".format( len(leptons), len(photons) ))
    #-- Dress leptons
    dressedLeptons = []
    anaTools.computeDressLepton(dressedLeptons, leptons, photons)

    #for i in range(len(dressedLeptons)):
    #    print("INFO  lepton a{}: pt={}, eta={}, phi={}, E={}, pdgId={}, frac={}, nPhotons={}".format(i, dressedLeptons[i].pt, dressedLeptons[i].eta, dressedLeptons[i].phi, dressedLeptons[i].E, dressedLeptons[i].pdgId, dressedLeptons[i].fracDressing, dressedLeptons[i].nPhotons) )


    #-- Sort leptons with descending pT
    dressedLeptons.sort(key=attrgetter('pt'), reverse=True)
    #sorted(dressedLeptons, key=lambda alep: alep.pt, reverse=True)
    #print("Number of dressed leptons: {}, Entry = {}".format( len(dressedLeptons), entry ))

    ##-- Select good muons and electrons
    #n_good_muons = 0
    #n_good_electrons = 0
    #good_muons = []
    #good_electrons = []
    #for lep in dressedLeptons: 
    #    if lep.pt > 15000.0:
    #        if (abs(lep.pdgId) == 13) and (abs(mu.eta()) < 2.4):
    #            good_muons.append(lep)
    #            n_good_muons = n_good_muons + 1
    #        if (abs(lep.pdgId) == 11) and ( (abs(el.eta()) < 1.37) 
    #                                        or (abs(el.eta()) > 1.52 and abs(el.eta()) < 2.47) ):
    #            good_electrons.append(lep)
    #            n_good_electrons = n_good_electrons + 1

    ##-- Sort electrons and muons in descending pT
    #sorted(good_muons, key=lambda a_muon: a_muon.pt, reverse=True)
    #sorted(good_electrons, key=lambda a_electron: a_electron.pt, reverse=True)

    #-- Require two good charged leptons (muon and/or electrons) with pT > 15GeV
    if len(dressedLeptons) < 2:
        continue;
    if dressedLeptons[0].pt < PT_LEP:
        continue;
    if dressedLeptons[1].pt < PT_LEP:
        continue;

    print("INFO  lepton 0: pt={}, eta={}, phi={}, E={}, pdgId={}, frac={}, nPhotons={}".format(dressedLeptons[0].pt, dressedLeptons[0].eta, dressedLeptons[0].phi, dressedLeptons[0].E, dressedLeptons[0].pdgId, dressedLeptons[0].fracDressing, dressedLeptons[0].nPhotons) )
    print("INFO  lepton 1: pt={}, eta={}, phi={}, E={}, pdgId={}, frac={}, nPhotons={}".format(dressedLeptons[1].pt, dressedLeptons[1].eta, dressedLeptons[1].phi, dressedLeptons[1].E, dressedLeptons[1].pdgId, dressedLeptons[1].fracDressing, dressedLeptons[1].nPhotons) )
    #if dressedLeptons[1].pt > dressedLeptons[0].pt:
    #    print("WARNING  Leading lepton has lower pT than subleading lepton: {} vs {}".format(dressedLeptons[0].pt, dressedLeptons[1].pt) )

    #-- Select jets
    good_jets = []
    anaTools.selectTruthJets(good_jets, tChain.AntiKt4TruthWZJets, dressedLeptons)
    # Sort jets in descending pT
    good_jets.sort(key=lambda a_jet: a_jet.pt, reverse=True)

    #-- Require atleast two good jets
    if (len(good_jets) < 2):
        continue;

    tcount = 0
    for jt in good_jets:
        print("INFO  Jet {}: pt={}, eta={}, phi={}, E={}, rapidity={}".format(tcount, jt.pt, jt.eta, jt.phi, jt.E, jt.rapidity) )
        tcount = tcount+1

    # Find gap jets
    gap_jets = []
    forwardJetRapidity = good_jets[0].rapidity
    backwardJetRapidity = good_jets[1].rapidity
    if forwardJetRapidity < backwardJetRapidity:
        forwardJetRapidity = good_jets[1].rapidity
        backwardJetRapidity = good_jets[0].rapidity
    for jet in good_jets[2:]:
        if (jet.rapidity < forwardJetRapidity) and (jet.rapidity > backwardJetRapidity):
            gap_jets.append(jet)

    if entry < 10:
        print("INFO  Number of good leptons = %g, good jets = %g and good gapJets = %g; mc_weight=%g" % (len(dressedLeptons), len(good_jets), len(gap_jets), mcWeight) )
         
    #-- Record info in output TTree
    weightColl.append(mcWeight)
    leptonColl.append(dressedLeptons)
    jetColl.append(good_jets)
    gapJetColl.append(gap_jets)


#-- Close dataset file
ds_file.Close()

###-----------------------------------------------------------------------------
### Creating histograms

#-- Create a global canvas
c1 = ROOT.TCanvas( 'c1', 'canvas1', 800, 600 )

# #-- Auto integer enumeration
# class AutoIntEnum(Enum):
#     def __new__(cls):
#         value = len(cls.__members__) + 1
#         el = int.__new__(cls)
#         el._value_ = value
#         return el
# 
# #-- Enumeration for registering histograms
# # This enum class gives the indices for the histograms 
# class indHist(AutoIntEnum):
#      nEvents = ()
#      nElec = ()
#      nMuon = ()
#      nLep = ()
#      nJet = ()
#      cutflow = ()
 
#-- Register histograms

###-----------------------------------------------------------------------------
# Create output tree and initialize leaves

#-- Open output file and create output tree
output_filename = 'truthStudy.root'
output_file = ROOT.TFile(output_filename, 'recreate')
otree = ROOT.TTree('truthCollection', 'Collection of truth objects')

#-- Check record objects
#for i in range( len(leptonColl) ):
#    v_leptons = leptonColl[i]
#    for j in range( len(v_leptons) ):
#        print("Check lepton: pt={}, eta={}, phi={}, pdgId={}".format(v_leptons[j].pt, v_leptons[j].eta, v_leptons[j].phi, v_leptons[j].pdgId))

#-- Parameters to store in an output TTree
# Number of electrons and muons to store in output TTree
NLEP_O = 3
# Number of jets to store in output TTree
NJET_O = 3
# Number of gap jets to store in output TTree
NGAPJET_O = 2
# Electrons or muons
lepton_pT = array.array('f', NLEP_O*[-1.0])
lepton_Eta = array.array('f', NLEP_O*[-99.0])
lepton_Phi = array.array('f', NLEP_O*[-99.0])
lepton_E = array.array('f', NLEP_O*[-1.0])
lepton_pdgId = array.array('i', NLEP_O*[-1])
lepton_fracDressing = array.array('f', NLEP_O*[-1.0])
lepton_nPhotons = array.array('i', NLEP_O*[-1])
# Jets
jet_pT = array.array('f', NJET_O*[-1.0])
jet_Eta = array.array('f', NJET_O*[-99.0])
jet_Phi = array.array('f', NJET_O*[-99.0])
jet_E = array.array('f', NJET_O*[-1.0])
jet_m = array.array('f', NJET_O*[-1.0])
jet_rapidity = array.array('f', NJET_O*[-1.0])
# Gap jets
gapjet_pT = array.array('f', NGAPJET_O*[-1.0])
gapjet_Eta = array.array('f', NGAPJET_O*[-99.0])
gapjet_Phi = array.array('f', NGAPJET_O*[-99.0])
gapjet_E = array.array('f', NGAPJET_O*[-1.0])
gapjet_m = array.array('f', NGAPJET_O*[-1.0])
gapjet_rapidity = array.array('f', NGAPJET_O*[-1.0])


#-- Declare parameters to store in TTree
#mc_weight = numpy.empty((1), dtype='float32')
mc_weight = array.array('f', [0.0])

#-- Declare TTree branches
otree.Branch('mc_weight', mc_weight, 'mc_weight/F')
# leptons
otree.Branch('lepton_pT', lepton_pT, 'lepton_pT[3]/F')
otree.Branch('lepton_Eta', lepton_Eta, 'lepton_Eta[3]/F')
otree.Branch('lepton_Phi', lepton_Phi, 'lepton_Phi[3]/F')
otree.Branch('lepton_E', lepton_E, 'lepton_E[3]/F')
otree.Branch('lepton_pdgId', lepton_pdgId, 'lepton_pdgId[3]/I')
otree.Branch('lepton_fracDressing', lepton_fracDressing, 'lepton_fracDressing[3]/F')
otree.Branch('lepton_nPhotons', lepton_nPhotons, 'lepton_nPhotons[3]/I')
# Jets
otree.Branch('jet_pT', jet_pT, 'jet_pT[3]/F')
otree.Branch('jet_Eta', jet_Eta, 'jet_Eta[3]/F')
otree.Branch('jet_Phi', jet_Phi, 'jet_Phi[3]/F')
otree.Branch('jet_E', jet_E, 'jet_E[3]/F')
otree.Branch('jet_m', jet_m, 'jet_m[3]/F')
otree.Branch('jet_rapidity', jet_rapidity, 'jet_rapidity[3]/F')
# Gap jets
otree.Branch('gapjet_pT', gapjet_pT, 'gapjet_pT[2]/F')
otree.Branch('gapjet_Eta', gapjet_Eta, 'gapjet_Eta[2]/F')
otree.Branch('gapjet_Phi', gapjet_Phi, 'gapjet_Phi[2]/F')
otree.Branch('gapjet_E', gapjet_E, 'gapjet_E[2]/F')
otree.Branch('gapjet_m', gapjet_m, 'gapjet_m[2]/F')
otree.Branch('gapjet_rapidity', gapjet_rapidity, 'gapjet_rapidity[2]/F')

#-- Fill branches
# Check size of containers
nWeight = len(weightColl)
nLeptons = len(leptonColl)
nJets = len(jetColl)
nGapJets = len(gapJetColl)

#print("Size of containers: weightColl={0}, leptonColl={1}, jetColl={2}, gapJetColl={3}".format(nWeight, nLeptons, nJets, nGapJets))
for i in range(nWeight):
    mc_weight[0] = weightColl[i]

    #print("Size of objects: lepton={}, jet={}, gapJet={}".format(len(leptonColl[i]), len(jetColl[i]), len(gapJetColl[i])))
    for j in range(NLEP_O):
        tmp_lep = anaTools.DressedLepton()
        if j < len(leptonColl[i]):
            tmp_lep = leptonColl[i][j]
        print("INFO  lepColl {}: pt={}, eta={}, phi={}, E={}, pdgId={}".format(j, tmp_lep.pt, tmp_lep.eta, tmp_lep.phi, tmp_lep.E, tmp_lep.pdgId) )

        lepton_pT[j] = tmp_lep.pt
        lepton_Eta[j] = tmp_lep.eta
        lepton_Phi[j] = tmp_lep.phi
        lepton_E[j] = tmp_lep.E
        lepton_pdgId[j] = tmp_lep.pdgId
        lepton_fracDressing[j] = tmp_lep.fracDressing
        lepton_nPhotons[j] = tmp_lep.nPhotons
        print("INFO  lepTree {}: pt={}, eta={}, phi={}, E={}, pdgId={}, frac={}, nPhotons={}".format(j, lepton_pT[j], lepton_Eta[j], lepton_Phi[j], lepton_E[j], lepton_pdgId[j], lepton_fracDressing[j], lepton_nPhotons[j]) )

    for k in range(NJET_O):
        #t_jet = ROOT.xAOD.Jet()
        tmp_jet = anaTools.Jet()
        if k < len(jetColl[i]):
            tmp_jet = jetColl[i][k] 
        print("INFO  JetColl {}: pt={}, eta={}, phi={}, E={}, m={}, rapidity={}".format(k, tmp_jet.pt, tmp_jet.eta, tmp_jet.phi, tmp_jet.E, tmp_jet.M, tmp_jet.rapidity) )

        #t_jetP4 = t_jet.p4() 
        #jet_pT[k] = t_jetP4.Pt()
        jet_pT[k] = tmp_jet.pt
        jet_Eta[k] = tmp_jet.eta
        jet_Phi[k] = tmp_jet.phi
        jet_E[k] = tmp_jet.E
        jet_m[k] = tmp_jet.M
        jet_rapidity[k] = tmp_jet.rapidity
        #
        print("INFO  JetTree {}: pt={}, eta={}, phi={}, E={},m={}, rapidity={}".format(k, jet_pT[k], jet_Eta[k], jet_Phi[k], jet_E[k], jet_m[k], jet_rapidity[k]) )

    for l in range(NGAPJET_O):
        tmp_gjet = anaTools.Jet()
        if l < len(gapJetColl[i]):
            tmp_gjet = gapJetColl[i][l] 

        gapjet_pT[l] = tmp_gjet.pt
        gapjet_Eta[l] = tmp_gjet.eta
        gapjet_Phi[l] = tmp_gjet.phi
        gapjet_E[l] = tmp_gjet.E
        gapjet_m[l] = tmp_gjet.M
        gapjet_rapidity[l] = tmp_gjet.rapidity

    # Fill TTree
    otree.Fill()

# Write and save file
output_file.Write()
output_file.Close()

# clear transient trees to avoid crash at end of job
ROOT.xAOD.ClearTransientTrees()
