
#-- Helper tools for VBF truth analysis
#


import ROOT
#ROOT.gROOT.Macro( '$ROOTCOREDIR/scripts/load_packages.C' )

#-- Class to save dressed leptons, which are lepton + photon within a cone of DR
# particle charge is given by the sign of pdgId
# fracDressing is the ratio of Pt(lepton+photons) to Pt(lepton)
class DressedLepton:
    def __init__(self):
        self.pt = 0.0
        self.eta = 0.0
        self.phi = 0.0
        self.E = 0.0
        self.pdgId = 0
        self.fracDressing = 0.0
        self.nPhotons = 0
    def setParameters(self, pt, eta, phi, energy, pid, frac=0.0, n_photons=0):
        self.pt = pt
        self.eta = eta
        self.phi = phi
        self.E = energy
        self.pdgId = pid
        self.fracDressing = frac
        self.nPotons = n_photons
    def getP4(self):
        tlv = ROOT.TLorentzVector()
        tlv.SetPtEtaPhiE(self.pt, self.eta, self.phi, self.E)
        return tlv

#-- Class to record jets
class Jet:
    def __init__(self):
        self.pt = 0.0
        self.eta = 0.0
        self.phi = 0.0
        self.M = 0.0
        self.E = 0.0
        self.rapidity = 0.0
    def setParameters(self, pt, eta, phi, mass, energy, rapidity):
        self.pt = pt
        self.eta = eta
        self.phi = phi
        self.M = mass
        self.E = energy
        self.rapidity = rapidity
    def getP4(self):
        tlv = ROOT.TLorentzVector()
        tlv.SetPtEtaPhiE(self.pt, self.eta, self.phi, self.E)
        return tlv

#-- Dress letons
def computeDressLepton(list_dressedLeptons, leptons, photons):
    DR = 0.1
    for lep in leptons:
        lepTLV = lep.p4()
        lep_pt = lepTLV.Pt()
        n_photons = 0
        for ph in photons:
            if (ph.p4()).DeltaR(lepTLV) < DR:
                lepTLV = lepTLV + ph.p4()
                n_photons = n_photons + 1
        # Save leptons
        t_lep = DressedLepton()
        frac_dressing = lepTLV.Pt()/lep_pt - 1.0
        t_lep.setParameters(lepTLV.Pt(), lepTLV.Eta(), lepTLV.Phi(), lepTLV.E(), 
                            lep.pdgId(), frac_dressing, n_photons)
        list_dressedLeptons.append(t_lep)

#-- Register integer leaves to store in Ttree
def registerLeafInteger(leafList, newleafName):
    leafList.append(newleafName)
    return;

#-- Register double leaves to store in Ttree
def registerLeafDouble(leafList, newleafName):
    leafList.append(newleafName)
    return;

#-- Check if particle is stable
def isParticleStable(particle):
    stable = False
    if (particle.status() == 1) and (particle.barcode() < 200000):
        stable = True
    return stable;

#-- Check if particle is either a lepton
def isLepton(pid):
    if (abs(pid) > 10) and (abs(pid) < 19):
        return True;
    return False;

#-- Check if particle is either a gauge boson
def isGaugeBoson(pid):
    if (abs(pid) > 21) and (abs(pid) < 38):
        return True;
    return False;

#-- Read a truthParticle collection and 
#   identify muons, electrons, photons
def identifyTruthLeptonsAndPhotons(leptons, photons, truthParticles):
    for ptcl in truthParticles:
        pdgId = ptcl.pdgId()

        #-- Check if particle is stable
        isStable = isParticleStable(ptcl)

        #-- Check if particle is a product of Z boson decay
        n_parent = int(ptcl.nParents())
        isFromZ = False
        for i in xrange(n_parent):
            if abs(ptcl.parent(i).pdgId()) == 23:
                isFromZ = True
                break

        #-- Check if particle is a product of lepton or weak gauge boson
        isWeakParticle = False
        for i in xrange(n_parent):
            pid = ptcl.parent(i).pdgId()
            if isLepton(pid) or isGaugeBoson(pid):
                isWeakParticle = True
                break

        #-- Select leptons and photons
        # TODO: Check if an isFromZ requirement would be beneficial
        if (isStable and isWeakParticle):
            # leptons
            if (abs(pdgId) == 13) or (abs(pdgId) == 11):
                leptons.append(ptcl)
            # Photons
            if abs(pdgId) == 22:
                photons.append(ptcl)

#-- Select good jets
# Requiring jet pt > 10 GeV
def selectTruthJets(truthJets, rawJets, dressedLeptons):
    # DR for overlap removal
    DR = 0.2
    for jet in rawJets:
        if jet.pt() > 10000.0:
            # Remove jets that are within DR=0.2 of an electron
            isElectron = False
            for lep in dressedLeptons:
                if (abs(lep.pdgId) == 11) and (lep.pt > 15000.0) \
                        and (lep.getP4().DeltaR(jet.p4()) < 0.2):
                    isElectron = True
                    break
            if isElectron:
                continue

            # save truth jet
            t_jet = Jet()
            t_jet.setParameters(jet.pt(), jet.eta(), jet.phi(), jet.m(), jet.e(), jet.rapidity())
            truthJets.append(t_jet)

